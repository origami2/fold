angular
.module(
  'neutrona.teme.topology',
  [
    'neutrona.teme.links',
    'neutrona.teme.topology.directive',
    'neutrona.teme.topology.api',
    'ui.router',
    'ngMaterial',
    'btford.socket-io'
  ]
)
.config([
  '$stateProvider',
  function ($stateProvider) {
    $stateProvider
    .state('topology-networks', {
      'url': '/topology',
      'views': {
        '': {
          'controller': 'TopologyNetworksPageController',
          'templateUrl': './src/topology/networks-page.html'
        }
      }
    })
    .state('topology', {
      'url': '/topology/:network',
      'views': {
        '': {
          'controller': 'TopologyPageController',
          'templateUrl': './src/topology/page.html'
        },
        'top-toolbar': {
          'templateUrl': './src/topology/top-toolbar.html',
          'controller': 'TopologyPageTopToolbarController'
        }
      }
    })
    .state('topology.isis-traffic', {
      'url': '/isis-traffic',
      'views': {
        'right-sidenav@': {
          'templateUrl': './src/topology/isis-traffic-right-sidenav.html',
          'controller': 'TopologyPageISISTrafficController'
        }
      }
    })
    .state('topology.node-detail', {
      'url': '/node-detail/:nodeId',
      'views': {
        'right-sidenav@': {
          'templateUrl': './src/topology/node-detail-right-sidenav.html',
          'controller': 'TopologyPageNodeDetailController'
        }
      }
    });
  }
])
.run([
  'AppLeftSidebarLinks',
  '$state',
  function (AppLeftSidebarLinks, $state) {
    AppLeftSidebarLinks.push({
      title: 'Topology',
      clicked: function () {
        $state.go('topology-networks');
      }
    });
  }
])
.controller(
  'TopologyPageController',
  [
    '$scope',
    'TopologyAPI',
    '$state',
    '$stateParams',
    'socketFactory',
    function ($scope, TopologyAPI, $state, $stateParams, socketFactory) {
      var socket = socketFactory();

      socket.on('topology-updated', function () {
        TopologyAPI
        .getGraph($stateParams.network)
        .then(function (topology) {
          $scope.topology = topology;
        });
      });

      $scope.$on('$destroy', function () {
        socket.disconnect();
      });

      TopologyAPI
      .getGraph($stateParams.network)
      .then(function (topology) {
        $scope.topology = topology;
      });

      $scope.$on('teme:topology:node-clicked', function (event, node) {
        $state
        .go(
          'topology.node-detail',
          {
            network: $stateParams.network,
            nodeId: node.data.id
          }
        );
      });
    }
  ]
)
.controller(
  'TopologyPageRightSidenavController',
  [
    '$scope',
    '$mdSidenav',
    function ($scope, $mdSidenav) {
      $scope.$on('teme:topology:show-node', function (event, node) {
        $scope.info = node;
        $mdSidenav('right').toggle('on');
      });
    }
  ]
)
.controller(
  'TopologyNetworksPageController',
  [
    '$scope',
    '$state',
    'TopologyAPI',
    function ($scope, $state, TopologyAPI) {
      TopologyAPI.listNetworks().then(function (networks) {
        $scope.networks = networks;
      });

      $scope.selectNetwork = function (n) {
        $state.go(
          'topology',
          {
            network: n.name
          }
        );
      };
    }
  ]
)
.controller(
  'TopologyPageTopToolbarController',
  [
    '$scope',
    '$mdBottomSheet',
    '$state',
    '$stateParams',
    'TopologyAPI',
    function ($scope, $mdBottomSheet, $state, $stateParams, TopologyAPI) {
     $scope.debugClicked = function () {
       $mdBottomSheet.show({
         templateUrl: './src/topology/debug-bottom-sheet.html',
         controller: 'TopologyPageDebugBottomSheetController'
       }).then(function(clickedItem) {
         switch(clickedItem) {
           case 'isis-traffic':
             $state.go(
               'topology.isis-traffic',
               {
                 network: $stateParams.network
               }
             );
             break;
           case 'recreate-from-isis-traffic':
             TopologyAPI.recreateFromIsisPackets($stateParams.network)
             break;
         }
       });
     }
   }
  ]
)
.controller(
  'TopologyPageDebugBottomSheetController',
  [
    '$scope',
    '$mdBottomSheet',
    function ($scope, $mdBottomSheet) {
      $scope.select = $mdBottomSheet.hide;
    }
  ]
)
.controller(
  'TopologyPageNodeDetailController',
  [
    '$scope',
    '$state',
    '$mdSidenav',
    '$stateParams',
    'TopologyAPI',
    function ($scope, $state, $mdSidenav, $stateParams, TopologyAPI) {
      $mdSidenav('right').toggle('on');

      TopologyAPI
      .getNodeInfo($stateParams.network, $stateParams.nodeId)
      .then(function (node) {
        $scope.info = node;
      });

      var opened = false;
      $scope.$watch(function () {
        return $mdSidenav('right').isOpen();
      }, function (isOpen) {
        if (!opened && isOpen) opened = true;
        if (opened && !isOpen) $state.go('^');
      });
    }
  ]
)
.controller(
  'TopologyPageISISTrafficController',
  [
    '$scope',
    '$mdSidenav',
    '$state',
    '$stateParams',
    'TopologyAPI',
    function ($scope, $mdSidenav, $state, $stateParams, TopologyAPI) {
      $mdSidenav('right').toggle('on');

      var opened = false;
      $scope.$watch(function () {
        return $mdSidenav('right').isOpen();
      }, function (isOpen) {
        if (!opened && isOpen) opened = true;
        if (opened && !isOpen) $state.go('^');
      });

      TopologyAPI
      .listIsisPackets($stateParams.network)
      .then(function (packets) {
        $scope.packets = packets;
      });

      $scope.showDetail = function (p) {
      }
    }
  ]
);

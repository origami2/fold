angular
.module(
  'neutrona.teme.topology.api',
  [
    'ng',
    'API'
  ]
)
.service(
  'TopologyAPI',
  [
    'API',
    function (API) {
      return API.Topology;
    }
  ]
);

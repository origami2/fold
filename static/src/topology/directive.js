angular
.module(
  'neutrona.teme.topology.directive',
  [
    'ngCytoscape'
  ]
)
.directive(
  'topologyMap',
  [
    function () {
      return {
        restrict: 'E',
        scope: {
          topology: '='
        },
        templateUrl: './src/topology/directive.html',
        controller: function ($scope, cytoData, $timeout) {
          var nodes = {};

          $scope.layout = { name: 'preset' };
          $scope.styles = [
            {
              selector: 'node',
              style: {
                'font-size': 8,
                'text-valign': 'top',
                'text-halign': 'center',
                'label': 'data(label)',
                'height': '9',
                'width': '9',
                'background-color': 'grey'
              }
            },
            {
              selector: '.pseudonode',
              style: {
                'height': '6',
                'width': '6',
                'background-color': '#bbb'
              }
            },
            {
              selector: '.invisible',
              style: {
                'height': '1',
                'width': '1'
              }
            },
            {
              selector: 'edge',
              style: {
                'text-valign': 'top',
                'text-halign': 'center',
                'label': 'data(label)',
                'width': 2
              }
            },
            {
              selector: '.down',
              style: {
                'line-color': '#c00'
              }
            }
          ];
          $scope.$watch('topology', function (json) {
            cytoData
            .getGraph('core')
            .then(function (graph) {
              // graph.startBatch();
              graph.remove(graph.edges());
              graph.remove(graph.nodes());

              if (!json) {
                $scope.elements = [];
                return;
              }

              if (json.elements) {
                $scope.elements = [].concat(json.elements.nodes || []).concat(json.elements.edges || []);

                nodes = {};
                for (var i = 0; i < json.elements.nodes.length; i++) {
                  var node = json.elements.nodes[i];
                  nodes[node.data.id] = node;
                }
              }
              else $scope.elements = [];
              // graph.stopBatch();

              graph.center();
            })
          });
          $scope.options = {
          };

          cytoData
          .getGraph('core')
          .then(function (graph) {
            $scope.$on('cy:node:mouseup', function(ng,cy){
              if (cy.cyTarget.isNode()) $scope.$emit('teme:topology:node-clicked', nodes[cy.cyTarget.id()]);
            });
          });
        }
      }
    }
  ]
)

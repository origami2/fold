(function(){
  'use strict';

  angular
  .module(
    'neutrona.teme.links',
    [
      'ng'
    ]
  )
  .value(
    'AppLeftSidebarLinks',
    []
  )
  .controller(
    'AppLeftSidebarController',
    [
      '$scope',
      'AppLeftSidebarLinks',
      function ($scope, AppLeftSidebarLinks) {
        $scope.links = AppLeftSidebarLinks;
      }
    ]
  );

  angular
  .module(
    'API',
    [
      'ng',
      'fold-api',
      'btford.socket-io'
    ]
  )
  .service(
    'socket',
    [
      'socketFactory',
      '$q',
      function (socketFactory, $q) {
        var queue = async.queue(function(task, callback) {
          socket.emit.apply(socket, task.args);

          callback();
        });

        queue.pause();

        var socket = socketFactory();

        socket.on('connect', function () {
          queue.resume();
        });

        var wrapper = {
          emit: function () {
            var args = [];
            for (var i = 0; i < arguments.length; i++) args.push(arguments[i]);

            queue.push({
              args: args
            });
          },
          on: socket.on
        };

        return wrapper;
      }
    ]
  )
  .service('API', [
    'socket',
    '$q',
    'FoldAPIDescription',
    '$rootScope',
    function (socket, $q, FoldAPIDescription, $rootScope) {
      function SocketInvocation(api, method, paramNames) {
          var context = {
            paramNames: paramNames,
            socket: socket,
            $q: $q,
            api: api
          };

          return (function () {
            var params = {};

            for (var i = 0; i < arguments.length; i++) {
              params[this.paramNames[i]] = arguments[i];
            }

            var defer = this.$q.defer();

            socket.emit('api', api, method, params, function (err, result) {
              if (err) return defer.reject(err);
              defer.resolve(result);
            });

            return defer.promise;
          }).bind(context);
      };

      var service = {
      };

      for (var api in FoldAPIDescription) {
        service[api] = {};

        for (var method in FoldAPIDescription[api]) {
          service[api][method] = new SocketInvocation(api, method, FoldAPIDescription[api][method]);
        }
      }

      socket.on('announce', function () {
        var args = [];
        for (var i = 0; i < arguments.length; i++) args.push(arguments[i]);

        $rootScope.$broadcast.apply($rootScope, arguments);
      });

      return service;
    }
  ]);

  angular
  .module(
    'neutrona.teme',
    [
      'ngMaterial',
      'neutrona.teme.links',
      'neutrona.teme.topology',
      'neutrona.teme.settings',
      'ui.router',
      'API'
    ]
  )
  .config(
    [
      '$stateProvider',
      function ($stateProvider) {
        $stateProvider
        .state('login', {
          views: {
            '': {
              controller: 'LoginController',
              template:''
            }
          }
        });
      }
    ]
  )
  .config(
    [
      '$mdThemingProvider',
      '$mdIconProvider',
      function($mdThemingProvider, $mdIconProvider) {
        $mdIconProvider
        .defaultIconSet("./assets/svg/avatars.svg"                    ,128)
        .icon("menu"          , "./assets/svg/menu.svg"               ,48)
        .icon("error"         , "./assets/svg/error.svg"              ,48)
        .icon("lock"          , "./assets/svg/lock.svg"               ,48)
        .icon("account_circle","./assets/svg/account_circle.svg"      ,48)
        .icon("add"           ,"./assets/svg/add.svg"                 ,48)
        .icon("verified_user" ,"./assets/svg/verified_user.svg"       ,48)
        .icon("autorenew" ,"./assets/svg/ic_autorenew_black_24px.svg" ,24);

        $mdThemingProvider
        .theme('default')
        .primaryPalette('green')
        .accentPalette('deep-purple');
      }
    ]
  )
  .run(
    [
      '$rootScope',
      '$state',
      '$location',
      'LoginService',
      function ($rootScope, $state, $location, LoginService) {
        LoginService.whoami().then(function (user) {
          if (!user) {
            $state.go('login', {
              backTo: $location.url()
            });
          }
        })
      }
    ]
  )
  .service(
    'LoginService',
    [
      '$http',
      '$q',
      '$rootScope',
      function ($http, $q, $rootScope) {
        return {
          login: function (username, password) {
            var p = $q.defer();

            $http
            .post(
              '/api/login',
              {
                username: username,
                password: password
              }
            )
            .success(p.resolve)
            .error(p.reject);

            return p.promise;
          },
          whoami: function () {
            var p = $q.defer();

            $http.get('/api/whoami')
            .success(p.resolve)
            .error(p.reject);

            return p.promise;
          }
        };
      }
    ]
  )
  .controller(
    'BodyController',
    [
      '$scope',
      '$mdSidenav',
      '$state',
      function ($scope, $mdSidenav, $state) {
        function toggleSidenav() {
          $mdSidenav('left').toggle();
        }

        $scope.go = function (section) {
          if (section === 'topology') return $state.go('topology');
        };

        $scope.toggleSidenav = toggleSidenav;
      }
    ]
  )
  .controller(
    'SidenavController',
    [
      '$scope',
      function ($scope) {
      }
    ]
  )
  .controller(
    'LoginController',
    [
      '$stateParams',
      'LoginService',
      '$rootScope',
      '$location',
      '$mdDialog',
      '$state',
      '$urlRouter',
      function ($stateParams, LoginService, $rootScope, $location, $mdDialog, $state, $urlRouter) {
        $mdDialog
        .show({
          controller: function ($scope, $mdDialog, LoginService) {
            $scope.confirm = function () {
              LoginService.login(
                $scope.credentials.username,
                $scope.credentials.password)
              .then(function () {
                $mdDialog.hide();
              }, function  (err) {
                $scope.error = err.error;
              });
            };
          },
          templateUrl: './src/view/credentials-dialog.html'
        })
        .finally(function () {
          $urlRouter.sync();
        });
      }
    ]
  );
})();

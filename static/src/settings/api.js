angular
.module(
  'neutrona.teme.networkSettings.api',
  [
    'ng',
    'API'
  ]
)
.service(
  'NetworkSettingsAPI',
  [
    'API',
    function (API) {
      return API.NetworkSettings;
    }
  ]
);

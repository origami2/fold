angular
.module(
  'neutrona.teme.settings',
  [
    'neutrona.teme.links',
    'neutrona.teme.networkSettings.api',
    'ui.router'
  ]
)
.run(
  [
    'AppLeftSidebarLinks',
    '$state',
    function (AppLeftSidebarLinks, $state) {
      AppLeftSidebarLinks.push({
        title: 'Settings',
        clicked: function () {
          $state.go('settings');
        }
      })
    }
  ]
)
.config(
  [
    '$stateProvider',
    function ($stateProvider) {
      $stateProvider
      .state(
        'settings',
        {
          'url': '/settings',
          'views': {
            '': {
              'templateUrl': './src/settings/page.html',
              'controller': 'SettingsPageController'
            }
          }
        }
      )
      .state(
        'network-settings',
        {
          'url': '/settings/network/:network',
          'views': {
            '': {
              'templateUrl': './src/settings/network.html',
              'controller': 'NetworkSettingsController'
            }
          }
        }
      )
    }
  ]
)
.service()
.controller(
  'SettingsNetworksController',
  [
    '$scope',
    '$state',
    'NetworkSettingsAPI',
    function ($scope, $state, NetworkSettingsAPI) {
      NetworkSettingsAPI.listNetworks()
      .then(function (networks) {
        $scope.networks = networks;
      });

      $scope.selectNetwork = function (n) {
        $state.go('network-settings', { network: n.name });
      };
    }
  ]
)
.controller(
  'SettingsPageController',
  [
    '$scope',
    'NetworkSettingsAPI',
    '$state',
    function ($scope, NetworkSettingsAPI, $state) {
      $scope.newNetwork = {};

      $scope.addNetwork = function () {
        var name = $scope.$eval('newNetwork.name');

        if (!name) return;

        NetworkSettingsAPI
        .create(name)
        .then(function () {
          $state.go('network-settings', { network: name });
        });
      }
    }
  ]
)
.controller(
  'NetworkSettingsController',
  [
    '$scope',
    '$mdDialog',
    '$stateParams',
    'NetworkSettingsAPI',
    '$mdToast',
    function ($scope, $mdDialog, $stateParams, NetworkSettingsAPI, $mdToast) {
      $scope.network = {
        name: $stateParams.network
      };

      function refreshClientToken() {
        NetworkSettingsAPI
        .getClientToken($stateParams.network)
        .then(function (clientToken) {
          $scope.clientToken = clientToken;
        });
      }

      $scope.regenerateClientToken = function () {
        NetworkSettingsAPI
        .regenerateClientToken($stateParams.network)
        .then(refreshClientToken);
      }

      refreshClientToken();

      NetworkSettingsAPI
      .isMonitoring($stateParams.network)
      .then(function (monitoring) {
        $scope.monitoring = monitoring;
      });

      $scope.monitoringChanged = function () {
        NetworkSettingsAPI
        .setMonitoring(
          $stateParams.network,
          $scope.monitoring
        );
      };

      function refreshRouters() {
        NetworkSettingsAPI
        .listKickstartRouters($stateParams.network)
        .then(function (routers) {
          $scope.kickstartRouters = routers;
        });
      }

      refreshRouters();

      $scope.removeKickstartRouter = function (routerName) {
        NetworkSettingsAPI
        .removeKickstartRouter(
          $stateParams.network,
          routerName
        )
        .then(function () {
          $mdToast.show(
            $mdToast
            .simple()
            .textContent('Kick-start router removed')
          );

          refreshRouters();
        }, function (err) {
          $mdToast.show(
            $mdToast
            .simple()
            .textContent('Error: ' + (err.error || err))
          );
        })
      };

      $scope.routerAuthentication = {};

      $scope.updateRouterAuthentication = function () {
        NetworkSettingsAPI
        .updateRouterAuthentication(
          $stateParams.network,
          $scope.routerAuthentication.username,
          $scope.routerAuthentication.privateKey
        )
        .then(function () {
          $mdToast
          .show(
            $mdToast
            .simple()
            .textContent('Router authentication settings updated')
          );
        }, function (err) {
          $mdToast.show(
            $mdToast
            .simple()
            .textContent('Error: ' + (err.error || err))
          );
        })
      };

      $scope.addKickstartRouter = function () {
        $mdDialog.show({
          locals: {
            network: $stateParams.network
          },
          controller: 'AddKickstartRouterController',
          templateUrl: './src/settings/add-router-dialog.html'
        })
        .then(function (router) {
          $mdToast.show(
            $mdToast
            .simple()
            .textContent('New kick-start router added')
          );
          refreshRouters();
        });
      };

      NetworkSettingsAPI
      .getLayoutUpdateSpacing($stateParams.network)
      .then(function (spacing) {
        $scope.spacing = spacing;

        $scope.$watch('spacing', function (n, o) {
          if (n === o) return;

          NetworkSettingsAPI
          .setLayoutUpdateSpacing($stateParams.network, n);
        });
      });
    }
  ]
)
.controller(
  'AddKickstartRouterController',
  [
    '$scope',
    '$mdDialog',
    'network',
    'NetworkSettingsAPI',
    '$mdToast',
    function ($scope, $mdDialog, network, NetworkSettingsAPI, $mdToast) {
      $scope.router = {};

      $scope.confirm = function () {
        NetworkSettingsAPI
        .addKickstartRouter(
          network,
          $scope.router.name,
          $scope.router.address
        )
        .then(
          function () {
            $mdDialog.hide();
          },
          function (err) {
            $mdToast.show(
              $mdToast
              .simple()
              .textContent('Error: ' + (err.error || err))
            );
          }
        );
      };
      $scope.cancel = function () {
        $mdDialog.hide();
      }
    }
  ]
)

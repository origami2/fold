var config = require('./config'),
    debug = require('debug')('origami-fold:' + config.fold);

debug('port: %s', config.port);
debug('ip: %s', config.ip);

var async = require('async');

async.auto({
  server: [
    'app',
    function (callback, results) {
      debug('Creating server');

      return callback (null, require('http').createServer(results.app));
    }
  ],
  app: [
    function (callback, results) {
      debug('Creating express.js app');

      var app = require('express')();

      var LdapAuth = require('ldapauth-fork');
      var config = require('config');

      var session = require('express-session');
      var MongoDBStore = require('connect-mongodb-session')(session);

      var sessionStore = new MongoDBStore(
        {
          uri: config.get('sessions.dbUrl'),
          collection: config.get('sessions.collection')
        }
      );

      app.use(
        session(
          {
            secret: config.get('sessions.secret'),
            cookie: {
              maxAge: config.get('sessions.age')
            },
            store: sessionStore
          }
        )
      );

      app.get(
        '/api/whoami',
        function (req, res, next) {
          res.json(req.session.user);
        }
      );

      app.post(
        '/api/login',
        [
          require('body-parser').json(),
          function (req, res, next) {
            if (!req.body) return next(new Error('Invalid data'));

            require('ldap-verifyuser')
            .verifyUser(
              {
                server: 'ldap://' + config.get('ldap.address') + ':',
                adrdn: config.get('ldap.domain') + '\\',
                adquery: config.get('ldap.query')
              },
              req.body.username,
              req.body.password,
              function (err, data) {
                if (err) return next(new Error('Invalid credentials'));

                if (!data.valid) return next(new Error('Invalid credentials'));

                req.session.user = req.body.username;
                res.status(200);
                res.end();
              }
            );
          },
          jsonError
        ]
      );

      function jsonError(err, req, res, next) {
        debug(err.message || err);
        res.status(503);
        res.json({
          error: err.message || err
        });
      }

      app.get('/fold-api-description.js', function (req, res) {
        res.header('Content-Type', 'application/javascript');

        results.stackClient.emit('describe-apis', function (err, apis) {
          var script = 'angular.module(\'fold-api\', [])\n' +
                       '.value(\'FoldAPIDescription\', ' + JSON.stringify(apis, undefined, 2) + ');';

          res.send(script);
        });
      });

      app.use(require('serve-static')(require('path').join(__dirname, 'static')));

      callback (null, app);
    }
  ],
  io: [
    'server',
    'stackClient',
    function (callback, results) {
      debug('Creating io server');

      var io = require('socket.io')(results.server);

      io.on('connection', function(socket){
        socket.on('api', function (api, method, params, callback) {
          debug('Passing api call %s.%s(%s)', api, method, JSON.stringify(params));

          try {
            results.stackClient.emit('api', api, method, params, callback);
          } catch (e) {
            debug('Error %s', e);
            callback (e);
          }
        });
      });

      callback (null, io);
    }
  ],
  stackClient: function (callback) {
    debug('Creating io stack client');

    var stackClient = require('socket.io-client')(config.stack, {
      timeout: 5000,
      reconnectionAttempts: 1
    });

    stackClient.on('connect_timeout', function () {
      callback('cannot connect to the stack');
    });

    stackClient.on('connect', function () {
      debug('connected to the stack');

      stackClient.on('identify', function (callback) {
        callback('fold', config.fold);
      });

      callback (null, stackClient);
    });
  }
}, function (err, results) {
  if (err) {
    debug(err);
    return process.exit(-1);
  }

  results.server.listen(config.port, config.ip, function () {
    debug('listening, browse to %s', config.publicAddress);
  });
});

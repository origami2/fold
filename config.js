var port = process.env.PORT || 3000,
    fold = process.env.FOLD || 'fold1';

module.exports = {
  port: port,
  ip: '0.0.0.0',
  publicAddress: 'http://localhost:' + String(port) + '/',
  stack: 'http://localhost:2000/',
  fold: fold
};
